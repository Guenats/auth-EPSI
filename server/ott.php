<?php

$salt_size = 40;

function test($username, $token){

    if(ott($username) == $token){
        echo 'Connecté en tant que '.$username;
    }else{
        echo 'non valide';
    }
}

function ott($username){
    return hash("sha256", getCurrentTimestamp() . getSaltUser($username) );
}

function getCurrentTimestamp(){
    $time = time();
    $timestamp = $time - ($time % 60);
    return $timestamp;
}

function getJson(){
    $txt = file_get_contents("json.json");
    $json = json_decode($txt, true);
    return $json;
}

function getIdUser($username){
    $json = getJson();

    foreach($json['users'] as $k=>$user){
        if($user['name'] == $username)
            return $k;
    }

    return -1;
}

function getSaltUser($username){
    $k = getIdUser($username);

    if($k != -1){
        $json = getJson();
        return $json['users'][$k]['salt'];
    }

    return null;
}

function getUserbySalt($salt){
    $json = getJson();

    foreach($json['users'] as $k=>$user){
        if($user['salt'] == $salt)
            return $user['name'];
    }

    return "";
}


$options = getopt("u:m:t:");

// get token
if($options["m"] == "generate" && isset($options["u"])){
    echo ott($options["u"])."\n";
}

if($options["m"] == "test" && isset($options["u"]) && isset($options["t"])){
    echo test($options["u"], $options["t"])."\n";
}


?>